#!/usr/bin/env python3
#
# Google Python Style Guide
# 
# module_name, package_name
# GLOBAL_CONSTANT_NAME, 
# global_var_name, local_var_name
# function_name, function_parameter_name
# ClassName, method_name, instance_var_name
# ExceptionName
#
from .abstract import Abstract, AbstractCommand
from logzero import logger


class Worker(Abstract):

    def __main__(self, args):
        logger.debug('Working')
        logger.info("Completed")


class WorkerCommand(AbstractCommand):
    worker = Worker();
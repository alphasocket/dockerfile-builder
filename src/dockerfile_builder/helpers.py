#!/usr/bin/env python3
#
# Globals
# 
# import globals and define generic functions
# 
import os
import re

from logzero import logger
from pathlib import Path
from collections import OrderedDict

import yaml
import requests
import subprocess as sp

SUBSHELL_REGEX = r'(\$\([^$(]+?\))'


def get_type(element):
    if type(element) is dict:
        return 'dict'
    elif type(element) is str:
        return 'str'
    elif type(element) is list:
        return 'list'
    else:
        return "type not recognized"


def list_to_dict(li):
    dct = {}
    index = 0
    for item in li:
        dct[index] = item
        index += 1
    return dct


def get_command_output(command):
    return sp.getoutput(command).rstrip('\n')


def run_command(command, logfile=None, errorlogfile=None):
    return 0 == get_exit_code(command, logfile, errorlogfile)


def get_exit_code(command, logfile=None, errorlogfile=None):
    if type(command) is dict:
        command = command.items()
    elif type(command) is str:
        if contains_nested_shell(command):
            command = run_nested_shell(command)
        command = command.split()
    elif type(command) is not list:
        logger.error("Command type not recognized")

    # Manage stout and stderr if specified
    log_file_path = error_file_path = None
    if logfile:
        log_file_path = get_path(logfile)
    if errorlogfile:
        error_file_path = get_path(errorlogfile)

    if log_file_path and error_file_path:
        with open(log_file_path, "a") as log:
            with open(error_file_path, "a") as err:
                child = sp.Popen(command, stdout=log, stderr=err)
    elif log_file_path:
        with open(log_file_path, "a") as log:
            child = sp.Popen(command, stdout=log)
    elif errorlogfile:
        with open(errorlogfile, "a") as err:
            child = sp.Popen(command, stderr=err)
    else:
        child = sp.Popen(command)

    # Run the command
    streamdata = child.communicate()[0]
    rc = child.returncode
    # print(command, log_file_path, error_file_path, rc)
    return rc


def contains_nested_shell(command):
    p = re.compile(SUBSHELL_REGEX)
    match = p.search(command)
    return bool(match)


def run_nested_shell(command):
    # Prepare the Regex
    p = re.compile(SUBSHELL_REGEX)
    # Catch the nested shell script
    match = p.search(command).group()
    match_command = match[2:-1]
    # Run
    result = get_command_output(match_command)
    # Replace $(...) with value
    command = command.replace(match, result)
    # If still contains a nested shell run this method again
    if contains_nested_shell(command):
        command = run_nested_shell(command)
    return command


def export_var(name, value):
    key = str(name).upper()
    value = str(value)
    os.environ[key] = value


def get_path(path):
    return get_command_output("echo {path}".format(path=path))


def file_exists(path):
    path = get_path(path)
    file = Path(path)
    return file.exists()


def get_realpath(path):
    if not file_exists(path):
        return False
    return get_command_output("realpath {path}".format(path=path))


def prepare_file(path):
    file = Path(path)
    dirname = Path(os.path.dirname(path))
    dirname_path = str(dirname)

    if not dirname.exists():
        try:
            # print(path, file,dirname)
            os.makedirs(str(dirname), 0o777)
        except:
            raise OSError("Can't create destination directory (%s)!" % (dirname_path))
    file.touch()


def parse_content(content):
    if type(content) in [dict, OrderedDict]:
        if 'valueFromFile' in content:
            return get_command_output("cat {file}".format(file=content['valueFromFile']))
        elif 'valueFromCommand' in content:
            # print(
            #     'valueFromCommand: ',
            #     content['valueFromCommand'],
            #     " => ",
            #     get_command_output(content['valueFromCommand'])
            # )
            return get_command_output(content['valueFromCommand'])
        elif 'valueFromParse' in content:
            # print(
            #     'valueFromParse: ',
            #     content['valueFromParse'],
            #     " => ",
            #     os.path.expandvars( content['valueFromParse'] ),
            #     " == ",
            #     get_command_output( 'echo "{0}"'.format(content['valueFromParse']))
            # )
            return os.path.expandvars(content['valueFromParse'])
        else:
            for key, value in content.items():
                content[key] = parse_content(value)
    elif type(content) in [list]:
        for index, item in enumerate(content):
            if type(item) in [dict, OrderedDict]:
                content[index] = parse_content(item)
    # elif type(content) in [str] and '$' in content :
    #    content = os.path.expandvars(content)

    return content


def is_parsable(content):
    if type(content) in [dict, OrderedDict]:
        if 'valueFromFile' in content:
            return True
        elif 'valueFromCommand' in content:
            return True
        elif 'valueFromParse' in content:
            return True
    return False


def get_data_from_yaml(yaml_file, ordered=False):
    yaml_file = get_path(yaml_file)
    if not os.path.exists(yaml_file):
        print("File not found in {0}".format(yaml_file))
        return False
    with open(yaml_file, 'r') as f:
        if ordered == True:
            return ordered_load(f, yaml.SafeLoader)
        else:
            return yaml.load(f)


def ordered_load(stream, Loader=yaml.Loader, object_pairs_hook=OrderedDict):
    class OrderedLoader(Loader):
        pass

    def construct_mapping(loader, node):
        loader.flatten_mapping(node)
        return object_pairs_hook(loader.construct_pairs(node))

    OrderedLoader.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        construct_mapping)
    return yaml.load(stream, OrderedLoader)


def get_http_response(url):
    r = {}

    try:
        r = requests.get(url)
    except requests.exceptions.HTTPError as err:
        print(err)
    except requests.exceptions.RequestException as err:
        print(err)

    return r


def get_url_head(url):
    r = {}

    try:
        r = requests.head(url, allow_redirects=True)
    except requests.exceptions.HTTPError as err:
        print(err)
    except requests.exceptions.RequestException as err:
        print(err)

    return r


def convert_date_to_epoch(date):
    return get_command_output("date --date='" + date + "' " + "'+%s'")


def get_size_of_file(path):
    return get_command_output('stat --printf="%s" {path}'.format(path=path))


def get_last_modification_time_of_file_in_seconds(path):
    return get_command_output('stat -c %Y {path}'.format(path=path))


def dict_compare(dict_a, dict_b):
    for i, j in zip(dict_a.items(), dict_b.items()):
        if i == j:
            return True
        else:
            return False


def dict_merge(dst, src):
    stack = [(dst, src)]

    while stack:
        current_dst, current_src = stack.pop()

        for key in current_src:
            if key not in current_dst:
                current_dst[key] = current_src[key]
            else:
                if isinstance(current_src[key], dict) and isinstance(current_dst[key], dict):
                    stack.append((current_dst[key], current_src[key]))
                else:
                    # print(key, current_dst[key], current_src[key])
                    current_dst[key] = current_src[key]
                # PUT THE VALUE AT THE END BECAUSE IT'S CHANGED
                tmp = current_dst[key]
                del (current_dst[key])
                current_dst[key] = tmp
    return dst


def verify_yaml_file(file_path):
    if yaml.safe_load(file_path):
        return True
    else:
        return False

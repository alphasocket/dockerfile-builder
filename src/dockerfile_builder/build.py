#!/usr/bin/env python3
#
# Google Python Style Guide
# 
# module_name, package_name
# GLOBAL_CONSTANT_NAME, 
# global_var_name, local_var_name
# function_name, function_parameter_name
# ClassName, method_name, instance_var_name
# ExceptionName
#
from .abstract import Abstract, AbstractCommand
from logzero import logger
import docker
import os
import git
import json


class Builder(Abstract):

    def __main__(self, args):
        self.import_config()
        log = ''
        result = True

        try:
            docker_client = docker.from_env()

            if os.path.exists(os.path.join(os.getcwd(), '.git')):
                try:
                    repo = git.Repo(os.getcwd())
                    if repo.is_dirty():
                        logger.warning("Repo is dirty, the commit in build doesn't match the code")
                except Exception as e:
                    logger.warning("Git repository error {}".format(e))

            logger.debug("Building image {}".format(self.tag))

            # Commented as it doesn't return the stream
            # https://docker-py.readthedocs.io/en/stable/images.html
            # https://stackoverflow.com/questions/43540254/how-to-stream-the-logs-in-docker-python-api
            # https://github.com/docker/docker-py/issues/1400
            #
            # image, log_stream = docker_client.images.build(
            #     path=os.getcwd(),
            #     nocache=True,
            #     pull=True,
            #     tag=self.tag,
            #     quiet=False,
            #     rm=True,
            #     labels=self.conf['build']['labels'],
            # )
            #
            # for line in log_stream:
            #     if 'stream' in line:
            #         line = str(line['stream']).strip()
            #         log += line + "\n"
            docker_client = docker.APIClient(
                base_url=os.environ['DOCKER_HOST']
            )

            for chunk in docker_client.build(
                    path=os.getcwd(),
                    nocache=True,
                    pull=True,
                    tag=self.tag,
                    quiet=False,
                    rm=True,
                    labels=self.conf['build']['labels'],
            ):
                json_output = json.loads(chunk.decode('ascii').strip('\r\n'))
                if 'error' in json_output:
                    raise Exception(chunk['error'])
                elif 'stream' in json_output:
                    output = json_output['stream'].strip('\n').strip('\r')
                    logger.info(output)

            logger.info("Image built successfully")

        except docker.errors.BuildError as e:
            logger.error("Build failed")
            logger.error(e)
            for line in e.build_log:
                if 'stream' in line:
                    log += str(line['stream']).strip() + "\n"
            result = False
        except Exception as e:
            logger.error(e)
            result = False

        if result:
            logger.debug(log)
        else:
            logger.error(log)

        return result


class BuildCommand(AbstractCommand):
    worker = Builder()

#!/usr/bin/env python3
#
# Google Python Style Guide
# 
# module_name, package_name
# GLOBAL_CONSTANT_NAME, 
# global_var_name, local_var_name
# function_name, function_parameter_name
# ClassName, method_name, instance_var_name
# ExceptionName
#
from .abstract import Abstract, AbstractCommand
from logzero import logger
import docker
import time


class Tester(Abstract):
    test_processes = []

    def run_test_processes(self, container):
        if self.conf['test']['processes']:
            for process in self.conf['test']['processes']:
                logger.debug("Testing process : {}".format(process['title']))
                for command in process['commands']:
                    if container.exec_run(cmd=command):
                        logger.info("Command successful: {}".format(command))
                    else:
                        logger.error("Command failed: {}".format(command))
                        return False
        else:
            logger.debug("No testing processes found")
        return True

    def __main__(self, args):
        conf_override = {}
        if type(args) == dict and 'conf' in args:
            conf_override = args['conf']
        self.import_config(conf_override)

        success = False
        logs = ""

        docker_client = docker.from_env()

        if not docker_client.images.list(name=self.name):
            from .build import Builder
            Builder.__main__()

        logger.debug('Testing build {}'.format(self.tag))

        ports = {}
        for port_type in ['main', 'additional']:
            if port_type in self.conf['test']['container']['ports'] \
                    and self.conf['test']['container']['ports'][port_type]:
                port = str(self.conf['test']['container']['ports'][port_type])
                if len(port) > 0:
                    ports[port] = int(self.conf['test']['host']['ports'][port_type])

        # Test service
        counter = 0
        timeout = int(self.conf['test']['timeoutSeconds'])
        interval = int(self.conf['test']['intervalSeconds'])
        initialDelay = int(self.conf['test']['initialDelaySeconds'])

        try:
            already_running = docker_client.containers.list(
                all=True,
                filters={
                    'name': str(self.conf['test']['container']['name'])
                }
            )
            if already_running:
                for zombie in already_running:
                    logger.debug("Stopping already running container")
                    zombie.stop()
                    zombie.remove()

            name = str(self.conf['test']['container']['name'])
            env = dict(self.conf['test']['container']['env'])
            volumes = list(self.conf['test']['container']['volumes'])
            if 'cmd' in self.conf['test']['container']:
                cmd = self.conf['test']['container']['cmd']
            else:
                cmd = self.conf['build']['envvars']['cmd']

            logger.debug(
                "Running container\nname: {}\nimage: {}\nenv: {}\nports: {}\nvolumes: {}\ncmd={} ".format(
                    name, self.tag, env, ports, volumes, cmd)
            )
            container = docker_client.containers.run(
                self.tag,
                cmd,
                remove=False,
                detach=True,
                ports=ports,
                environment=env,
                name=name,
                volumes=volumes,
                working_dir=str(self.conf['test']['container']['workdir'])
            )

            # Giving time to system to start container
            time.sleep(initialDelay)
            while not success and counter < timeout:
                if not container.reload() and container.status == 'exited':
                    result = dict(container.wait())
                    if result['StatusCode'] == 0:
                        # Command executed
                        success = True
                        logger.info("Container successfully exited: {}".format(result))
                    else:
                        logger.error("Container failed: {}".format(result))
                    break
                elif self.run_test_processes(container):
                    success = True
                    break
                else:
                    time.sleep(interval)
                    counter = counter + 1

            if counter == timeout:
                logger.debug("Timeout reached")

            container.stop()

            logs = container.logs()

            container.remove()

        except Exception as e:
            logger.error("Test failed {}".format(e))

        if success:
            logger.info(logs)
            logger.info("Test successful")
            return True
        else:
            logger.error(logs)
            logger.error("Test failed")
            return False


class TestCommand(AbstractCommand):
    worker = Tester()

#!/usr/bin/env python3
#
# Google Python Style Guide
# 
# module_name, package_name
# GLOBAL_CONSTANT_NAME, 
# global_var_name, local_var_name
# function_name, function_parameter_name
# ClassName, method_name, instance_var_name
# ExceptionName
#
import os
import git

from .helpers import *
from .abstract import Abstract, AbstractCommand
from .compile import CompileCommand

from logzero import logger


class Deployer(Abstract):

    def __main__(self, args):
        self.import_config()

        logger.debug('Deploy changes from master to branches:')
        g = git.Git(os.getcwd())
        repo = git.Repo(os.getcwd())
        if 3 in args:
            m=args[3]
        else:
            m='Deploy changes from master to branches'

        g.checkout("master", force=True)
        g.pull()

        old_bin = os.getcwd() + '/bin/deploy_branches'
        if os.path.isfile(old_bin):
            os.remove(old_bin)
        
        if not g.status():
            logger.debug('Committing...')
            repo.index.add('.')
            repo.index.commit(m=m)
            logger.debug('Pushing...')
            repo.push('origin', "master")

        repo_heads = repo.heads
        repo_heads_names = [h.name for h in repo_heads]

        for branch in repo_heads_names:
            if branch not in ['master','develop']:
                logger.debug('Merging changes in branch {0}'.format(branch))
                g.checkout(branch)
                g.merge('master')
                logger.debug('Compiling branch {0}...'.format(branch))
                b = CompileCommand()
                b.__main__(args)
                logger.debug('Committing...')
                repo.index.add('.')
                repo.index.commit("Compiled dockerfile and required files")
                logger.debug('Pushing...')
                repo.remotes.origin.push(branch)
        
        logger.info("Completed")


class DeployCommand(AbstractCommand):
    worker = Deployer()


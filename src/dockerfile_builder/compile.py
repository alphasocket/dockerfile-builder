#!/usr/bin/env python3
#
# Google Python Style Guide
# 
# module_name, package_name
# GLOBAL_CONSTANT_NAME, 
# global_var_name, local_var_name
# function_name, function_parameter_name
# ClassName, method_name, instance_var_name
# ExceptionName
#
from .helpers import *
from .abstract import Abstract, AbstractCommand
from logzero import logger
import os


class Compiler(Abstract):
    docker_imports = []

    def eval_template(self, template, mode=None):

        template_file = os.path.join(
            self.package_folder,
            self.conf['builder']['folders']['templates'],
            self.conf['builder']['templates'][template]
        )

        target_file = os.path.join(os.getcwd(), os.environ['BUILDER_TARGETS_BUILD_{0}'.format(template.upper())])

        prepare_file(target_file)
        target_handler = open(target_file, "w");

        with open(template_file, "r") as template_file_handler:
            content = os.path.expandvars(template_file_handler.read())
            if template == "test":
                content = os.path.expandvars(content)
            target_handler.write(content)

        target_handler.close()
        template_file_handler.close()

        if mode is not None:
            os.chmod(target_file, mode);

    def rm_template_target(self, template):
        target_file = "{pwd}/{template_target}".format(
            pwd=os.getcwd(),
            template_target=os.environ['BUILDER_TARGETS_BUILD_{0}'.format(template.upper())]
        )
        if target_file and os.path.isfile(target_file):
            os.remove(target_file)

    def import_files(self):
        self.import_stage_files('builder')
        self.import_stage_files('build')

    def __main__(self, args):

        self.import_config()

        # Import files
        logger.debug('Importing files in build')
        self.import_files()
        # Rebuild builder_env after pulling files
        self.build_builder_env()

        logger.debug('Building files')
        cur_branch = os.environ['BUILD_BRANCH']
        if cur_branch not in self.conf['builder']['branch2templates']:
            cur_branch = 'default'
        other_branches = list(self.conf['builder']['branch2templates'].keys())
        other_branches.remove(cur_branch)

        # Remove other branch templates
        for other_branch in other_branches:
            for template in self.conf['builder']['branch2templates'][other_branch]:
                self.rm_template_target(template['key'])

        # Create cur branch templates
        for template in self.conf['builder']['branch2templates'][cur_branch]:
            key = template['key']
            if 'mode' in template:
                mode = int(str(template['mode']), 8)
            else:
                mode = None
            self.eval_template(key, mode)

        logger.info("Dockerfile Compiled")
        return True


class CompileCommand(AbstractCommand):
    worker = Compiler()

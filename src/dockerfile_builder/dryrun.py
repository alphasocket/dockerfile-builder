#!/usr/bin/env python3
#
# Google Python Style Guide
# 
# module_name, package_name
# GLOBAL_CONSTANT_NAME, 
# global_var_name, local_var_name
# function_name, function_parameter_name
# ClassName, method_name, instance_var_name
# ExceptionName
#

from logzero import logger
from .abstract import AbstractCommand
from .compile import Compiler
from .build import Builder
from .test import Tester


class DryrunCommand(AbstractCommand):
    result = False

    def __main__(self, args):

        try:
            if True \
                    and Compiler().__main__(args) \
                    and Builder().__main__(args) \
                    and Tester().__main__(args):
                logger.info('Dryrun completed successfully')
                self.result = True
            else:
                logger.error('Dryrun failed')
                self.result = False
        except Exception as e:
            logger.error('Dryrun failed .{}'.format(e))
            self.result = False

        return self.result

#!/usr/bin/env python3

from collections import OrderedDict


def dict_compare(dict_a, dict_b):
    return dict_a == dict_b


def dict_merge(dst, src):
    return {**dst, **src}
    z = dst.copy()
    z.update(src)
    return z

    stack = [(dst, src)]

    while stack:
        current_dst, current_src = stack.pop()

        for key in current_src:
            if key not in current_dst:
                current_dst[key] = current_src[key]
            else:
                if isinstance(current_src[key], dict) and isinstance(current_dst[key], dict):
                    stack.append((current_dst[key], current_src[key]))
                else:
                    # print(key, current_dst[key], current_src[key])
                    current_dst[key] = current_src[key]
                # PUT THE VALUE AT THE END BECAUSE IT'S CHANGED
                tmp = current_dst[key]
                del (current_dst[key])
                current_dst[key] = tmp
    return dst

a = OrderedDict({ 'a': 1, 'b':2 })
b = OrderedDict({ 'b': 3 })

c = dict_merge(a, b)
test = OrderedDict({ 'b': 3, 'a':1 })

if c == test:
    print('Test succeded')
else:
    print('Test failed')
    print( c, ' != ', test)


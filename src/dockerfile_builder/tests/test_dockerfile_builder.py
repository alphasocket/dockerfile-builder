import pytest
import dockerfile_builder


def test_project_defines_author_and_version():
    assert hasattr(dockerfile_builder, '__author__')
    assert hasattr(dockerfile_builder, '__version__')

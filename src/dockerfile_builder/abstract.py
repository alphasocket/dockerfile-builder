#!/usr/bin/env python3
#
# Google Python Style Guide
# 
# module_name, package_name
# GLOBAL_CONSTANT_NAME, 
# global_var_name, local_var_name
# function_name, function_parameter_name
# ClassName, method_name, instance_var_name
# ExceptionName
#
import os
from .helpers import *
from logzero import logger


class Abstract:
    package_folder = os.path.dirname(os.path.realpath(__file__))
    conf = OrderedDict()
    envvars = OrderedDict()
    docker_envvars = []
    docker_imports = []
    unparsed_variables = {}

    def __init__(self):
        pass

    def resolve_env_var(self, envvars):
        result = OrderedDict()
        for key, value in envvars.items():
            variable_name = str(key).upper()

            if type(value) not in [dict, OrderedDict]:
                result[variable_name] = value
            elif is_parsable(value):
                result[variable_name] = value
            else:
                prefix = variable_name + "_"
                resolved_vars = self.resolve_env_var(value)
                for key, value in resolved_vars.items():
                    variable_name = str(key).upper()
                    result[prefix + variable_name] = value
        return result

    def import_stage_env(self, stage, parse=True):
        # CHeck if all var are parsed
        all_parsed = True
        if 'envvars' in self.conf[stage]:
            envvars = self.conf[stage]['envvars']
            envvars = self.resolve_env_var(envvars)
            concat = ""

            for name, value in envvars.items():
                name = "{stage}_{name}".format(stage=str(stage).upper(), name=name)
                if value is None:
                    value = ""
                if parse is True:
                    # print('parsing var: ',name,value, is_parsable(value), parse_content(value) )
                    if is_parsable(value):
                        if 'valueFromCommand' in value:
                            unparsed_value = value['valueFromCommand']
                        elif 'valueFromParse' in value:
                            unparsed_value = value['valueFromParse']
                        else:
                            unparsed_value = ""
                        # print('Unparsed and value: ',unparsed_value,value)
                        parsed_value = parse_content(value)

                        if unparsed_value != parsed_value and 'OrderedDict' not in str(parsed_value):
                            value = parsed_value
                            if name in self.unparsed_variables:
                                del (self.unparsed_variables[name])
                            export_var(name, value)
                        else:
                            self.unparsed_variables[name] = unparsed_value
                            all_parsed = False
                    else:
                        export_var(name, value)

                # print('exporting var: ',name,value)

                # Keep a list of stage envvars in builder envvars
                if stage not in self.envvars:
                    self.envvars[stage] = OrderedDict()
                self.envvars[stage][name] = value
                concat = concat + '{name}="{value}"\n'.format(name=name, value=value)

                if stage in ['general', 'build', 'setup', 'config']:
                    # Workaround quotes
                    if type(value) is str and value.startswith("'") and value.endswith("'"):
                        # Single quotes
                        self.docker_envvars.append("{name}={value}".format(name=name, value=value))
                    else:
                        # Double quotes
                        self.docker_envvars.append('{name}="{value}"'.format(name=name, value=value))
                    # Workaround strings

            export_var('DOCKERFILE_BUILDER_{stage}_ENVVARS'.format(stage=stage.upper()), concat)
        return all_parsed

    def build_all_stage_processes(self, stage, commands_prefix="", row_prefix=""):
        if 'processes' in self.conf[stage]:
            # Gather processes
            processes = self.conf[stage]['processes'];
            if 'processes_before' in self.conf[stage]:
                processes = self.conf[stage]['processes_before'] + processes;
            if 'processes_after' in self.conf[stage]:
                processes = processes + self.conf[stage]['processes_after'];

            # Concat processes
            imploded_processes = self.implode_processes(processes, commands_prefix, row_prefix, stage)

            # Expand env vars in test files ( Outside container env )
            if self.conf['stages'][stage]['expand_vars']['processes'] == True:
                imploded_processes = os.path.expandvars(imploded_processes)

            # Export var to env
            export_var(
                '{stage}_PROCESSES'.format(stage=stage.upper()),
                imploded_processes
            )

    def implode_processes(self, processes, commands_prefix="", row_prefix="", stage_name=""):
        concat = ""

        for process in processes:
            concat += "# {title}\n".format(title=process['title'])
            concat += '[ "${stage_name}_VERBOSE" = "$GENERAL_TRUE" ] || echo "### {title}..."\n'.format(
                title=process['title'], stage_name=stage_name.upper())

            commands = else_commands = ""

            for command in process['commands']:
                if 'shell_condition' in process:
                    commands += "    {row_prefix}".format(row_prefix=row_prefix)
                    # Add final ; only if necessary
                    if command.endswith(';'):
                        command_suffix = '\n'
                    else:
                        command_suffix = ';\n'
                else:
                    commands += commands_prefix
                    command_suffix = '\n'

                commands += "{command}{command_suffix}".format(
                    command=command,
                    command_suffix=command_suffix
                )

            if 'else' in process:
                else_commands = "else \n    "
                for else_command in process['else']:
                    else_command_prefix = "    "
                    # Add final ; only if necessary
                    if else_command.endswith(';'):
                        else_command_suffix = '\n'
                    else:
                        else_command_suffix = ';\n'
                    else_commands += "{else_command_prefix}{else_command}{else_command_suffix}".format(
                        else_command_prefix=else_command_prefix,
                        else_command=else_command,
                        else_command_suffix=else_command_suffix
                    )

            if 'shell_condition' in process:
                string = "{commands_prefix}if [ {condition} ]; then\n{commands}{else_commands}{row_prefix}fi\n"
                concat += string.format(
                    commands_prefix=commands_prefix,
                    condition=process['shell_condition'],
                    commands=commands,
                    else_commands=else_commands,
                    row_prefix=row_prefix
                )
            else:
                concat += commands + "\n"
        return concat

    def build_builder_env(self):
        # Build args
        docker_args = ""
        build_args = ""
        if 'args' in self.conf['build']:
            for arg, value in self.conf['build']['args'].items():
                docker_args += "ARG BUILD_{0}\n".format(arg.upper())
                build_args += "--build-arg BUILD_{0}={1} ".format(arg.upper(), value)
        export_var('DOCKERFILE_BUILDER_ARGS', docker_args)
        export_var('BUILD_ARGS', build_args)

        # Build env file
        docker_env = "ENV"
        for envvar in self.docker_envvars:
            docker_env += " \\\n\t" + envvar
        export_var('DOCKERFILE_BUILDER_ENVVARS', docker_env)

        # Expose ports
        docker_ports = ""
        if 'main' in self.conf['build']['envvars']['ports'] and self.conf['build']['envvars']['ports']['main']:
            docker_ports = "EXPOSE {main} {additional}".format(
                main=self.conf['build']['envvars']['ports']['main'],
                additional=self.conf['build']['envvars']['ports']['additional']
            )
        export_var('DOCKERFILE_BUILDER_PORTS', docker_ports)

        # WORKDIR
        workdir = ""
        if 'workdir' in self.conf['build']['envvars']:
            workdir = 'WORKDIR {workdir}'.format(workdir=os.environ['BUILD_WORKDIR'])
        export_var('DOCKERFILE_BUILDER_WORKDIR', workdir)

        # IMPORTS
        concat = ""
        # logger.debug(self.docker_imports)
        for imported in self.docker_imports:
            concat = "{concat}ADD {source} {target}\n".format(
                concat=concat,
                source=imported['source'],
                target=imported['target']
            )
        # Exporting env variable
        export_var('DOCKERFILE_BUILDER_IMPORTS', concat)

        # CACHE docker images
        export_var('DOCKERFILE_BUILDER_CACHE_DOCKER_IMAGES',
                   os.path.expandvars(' '.join(self.conf['cache']['docker_images'])))

    def build_project_env(self):
        os.environ['PROJECT_TITLE'] = self.conf['project']['title']
        os.environ['PROJECT_CODENAME'] = self.conf['project']['codename']
        os.environ['PROJECT_DESCRIPTION'] = self.conf['project']['description']

        # Format versions
        branches = get_command_output("git for-each-ref --format='%(refname:short)' refs/heads/").split("\n")
        concat = ""
        for line in branches:
            if "master" != line:
                concat += "- {line}\n".format(line=line)
        os.environ['PROJECT_VERSIONS'] = concat

        # Format dependencies
        os.environ['PROJECT_PACKAGES'] = ""
        for stage in ['setup', 'config', 'runtime']:
            label = 'SETUP_DEPENDENCIES_{ucstage}'.format(ucstage=stage.upper())
            if label in os.environ:
                dependencies = str(os.environ[label]).strip().split()
                if dependencies:
                    os.environ['PROJECT_PACKAGES'] += "- {stage} dependencies:\n".format(stage=stage.title())
                    for package in dependencies:
                        if not bool(package):
                            package = 'None'
                        os.environ['PROJECT_PACKAGES'] += "  + {package}\n".format(package=package)

    def import_envvars(self):
        self.build_project_env()
        self.build_builder_env()

        # Import env var until all unparsed values are parsed
        all_parsed = False
        counter = 0
        while all_parsed is False:
            counter += 1
            # Put a limit to while cycle
            if counter >= 15:
                for name, unparsed_value in self.unparsed_variables.items():
                    logger.error("Error: can't parse value \"{}\" for {} ".format(unparsed_value, name))
                exit(1)
            # Reset docker_envvars
            self.docker_envvars = []
            # Import var parsing data correctly (having already the value for early run)
            all_parsed = True

            for stage in list(self.conf['stages'].keys()):
                result = self.import_stage_env(stage)
                if result is False:
                    all_parsed = False

    def build_folder_imports(self, source_path, build_path):
        tree = []
        source_path_lenght = len(source_path)
        for dirpath, dirnames, filenames in os.walk(source_path):

            for filename in filenames:
                # Replacing source patch with build path in dirpath
                build_dirpath = build_path + dirpath[source_path_lenght:]

                file_source_path = dirpath + '/' + filename
                file_build_path = build_dirpath + '/' + filename
                tree.append([file_source_path, file_build_path])
        return tree

    def import_stage_files(self, stage):
        if 'imports' in self.conf[stage]:
            files = self.conf[stage]['imports']

            if type(files) is list:
                files = list_to_dict(files)

            # Breaking format source:target
            for key, value in files.items():
                value = os.path.expandvars(value)
                paths = value.split(':')

                source_path = paths[0]
                build_path = paths[1]

                prefix = os.environ['BUILDER_TARGETS_FOLDERS_BUILD_IMPORTS'] + '/'

                ## Add file by file to not override previous injected files
                if source_path and build_path:
                    # Source is file
                    if os.path.isfile(source_path):
                        self.docker_imports.append({"source": source_path, "target": build_path})
                    # Source is dir
                    else:
                        tree = self.build_folder_imports(source_path, build_path);
                        for file_source_path, file_build_path in tree:
                            self.docker_imports.append({"source": file_source_path, "target": file_build_path})
                # logger.debug(self.docker_imports);

    def import_files(self):
        self.import_stage_files('builder')
        self.import_stage_files('build')

    def build_processes(self):
        self.build_all_stage_processes('setup')
        self.build_all_stage_processes('config')
        self.build_all_stage_processes('harden')
        self.build_all_stage_processes('test')
        self.build_all_stage_processes('entrypoint_once')
        self.build_all_stage_processes('entrypoint')

    def import_config(self, conf_override=None):
        config_yaml_path = os.path.join(self.package_folder, 'config', "main.yaml")
        configurations = get_data_from_yaml(config_yaml_path, True)
        self.conf = OrderedDict(configurations)
        # import pprint
        # pp = pprint.PrettyPrinter(indent=0)
        # pp.pprint( self.conf )

        # Import build config
        build_yaml = get_path("{}/{}".format(os.getcwd(), self.conf['file']['name']))
        if not os.path.exists(build_yaml):
            logger.error("File {} not found".format(self.conf['file']['name']))
            exit(1)

        # Import configurations
        build_config = get_data_from_yaml(build_yaml, True)
        # Merge build over defaults
        build_config = dict_merge(self.conf['defaults'], build_config)
        # Import build conf in conf
        self.conf = dict_merge(self.conf, build_config)
        # Import override
        if type(conf_override) == dict:
            self.conf = dict_merge(self.conf, conf_override)
        self.conf = dict_merge(self.conf, self.conf['finals'])

        self.conf['build']['envvars']['maintainer'] = self.conf['project']['author']
        #
        # IMPORT
        #
        # Build env vars for every stage
        logger.debug('Importing enviroment variables')
        self.import_envvars()

        # Build setup processes
        logger.debug('Importing processes')
        self.build_processes()
        # Parse Vars
        self.conf = parse_content(self.conf)

        # Build vars
        self.name = "{}/{}".format(
            self.conf['test']['docker']['user'],
            self.conf['build']['envvars']['name']
        )
        self.version = self.conf['build']['envvars']['commit']
        self.tag = "{}:{}".format(self.name, self.version)

        # Push Vars
        push_user = self.conf['test']['docker']['user']
        if str(self.conf['push']['docker']['user']):
            push_user = self.conf['push']['docker']['user']
        self.registry = self.conf['push']['docker']['registry']
        self.repo = "{}/{}/{}".format(
            self.registry,
            push_user,
            self.conf['build']['envvars']['name']
        )


class AbstractCommand:

    def __init__(self):
        pass

    def __main__(self, args):
        return self.worker.__main__(args)

#!/usr/bin/env python3
#
# Google Python Style Guide
# 
# module_name, package_name
# GLOBAL_CONSTANT_NAME, 
# global_var_name, local_var_name
# function_name, function_parameter_name
# ClassName, method_name, instance_var_name
# ExceptionName
#
from .abstract import Abstract, AbstractCommand
from logzero import logger
import docker


class Pusher(Abstract):

    def __main__(self, args):
        self.import_config()

        try:
            docker_client = docker.from_env()
            if self.conf['push']['docker']['login'] == self.conf['general']['envvars']['keys']['true']:
                logger.debug('Logging into registry {}'.format(self.registry))
                docker_client.login(
                    registry=self.conf['push']['docker']['registry'],
                    username=self.conf['push']['docker']['user'],
                    password=self.conf['push']['docker']['pass'],
                )

            image = docker_client.images.get(self.tag)
            for tag in list(self.conf['push']['tags']):
                logger.debug('Tagging image {} to {}:{}'.format(self.tag, self.repo, tag))
                image.tag(self.repo, tag)
                logger.debug('Pushing image to {}'.format(self.repo))
                for line in docker_client.images.push(self.repo, tag, stream=True, decode=True):
                    logger.debug(line)
                    if 'errorDetail' in line:
                        logger.error('Cannot push image {}'.format(line['errorDetail']))
                        return False

                logger.info('Image pushed with tag {}'.format(tag))
            return True

        except docker.errors.APIError as e:
            logger.error('Cannot push image {}'.format(e))
            return False


class PushCommand(AbstractCommand):
    worker = Pusher()

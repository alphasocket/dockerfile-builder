# ${PROJECT_TITLE}
#### ${PROJECT_CODENAME}
${PROJECT_DESCRIPTION}

## Branches & Versions
${PROJECT_VERSIONS}

## Packages installed
${PROJECT_PACKAGES}

## Configurable envvars
~~~
${DOCKERFILE_BUILDER_CONFIG_ENVVARS}~~~

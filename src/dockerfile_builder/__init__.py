#!/usr/bin/env python3
import sys
import argparse
from functools import reduce
from .compile import CompileCommand
from .build import BuildCommand
from .deploy import DeployCommand
from .test import TestCommand
from .push import PushCommand
from .dryrun import DryrunCommand
from .pipeline import PipelineCommand

__author__ = 'Alphasocket'
__version__ = '0.3.0'


def str_to_class(str):
    return reduce(getattr, str.split("."), sys.modules[__name__])


def main(args):
    if len(args) < 2:
        print('Specify an action')
        return False

    command = '{action}Command'.format(action=args[1].capitalize())
    
    try:
        cls = str_to_class(command)
    except AttributeError:
        cls = None

    if cls:
        return cls.__main__(cls, args)
    else:
        print('Command not recognized');
        return False


if __name__ == "__init__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser()

    # Required positional argument
    parser.add_argument("arg", help="Required positional argument")

    # Optional argument flag which defaults to False
    parser.add_argument("-f", "--flag", action="store_true", default=False)

    # Optional argument which requires a parameter (eg. -d test)
    parser.add_argument("-n", "--name", action="store", dest="name")

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Verbosity (-v, -vv, etc)")

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    parser.add_argument("action", help="Required positional argument")

    args = parser.parse_args()
    
    main(args)

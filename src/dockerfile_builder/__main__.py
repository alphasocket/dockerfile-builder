import re
import sys
import dockerfile_builder

if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw?|\.exe)?$', '', sys.argv[0])
    if not dockerfile_builder.main(sys.argv):
        sys.exit(1)
    else:
        sys.exit(0)

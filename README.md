# dockerfile-builder
#### dockerfile-builder
This docker image containing the python module dockerfile-builder used to build Docker images

## Branches & Versions
- develop
- latest


## Packages installed
- Setup dependencies:
  + gcc
  + py-setuptools
  + openssl-dev
  + jansson-dev
  + python-dev
  + build-base
  + libc-dev
  + file-dev
  + automake
  + autoconf
  + libtool
  + flex
  + linux-headers
  + ghc
  + musl-dev
  + zlib-dev
  + curl
  + bind-tools
- Runtime dependencies:
  + bash
  + bash-doc
  + bash-completion
  + coreutils
  + man
  + man-pages
  + mdocml-apropos
  + vim
  + grep
  + less
  + less-doc
  + curl
  + zip
  + unzip
  + jq
  + htop
  + git
  + mercurial
  + gnupg
  + fcgi
  + openssh-client
  + ansible
  + file
  + tmux
  + tree
  + xz
  + gzip
  + libxml2
  + libxml2-utils
  + libc6-compat
  + php7
  + php7-curl
  + php7-dom
  + php7-gd
  + php7-ctype
  + php7-gettext
  + php7-iconv
  + php7-json
  + php7-mbstring
  + php7-mcrypt
  + php7-mysqli
  + php7-opcache
  + php7-openssl
  + php7-pdo
  + php7-pdo_dblib
  + php7-pdo_mysql
  + php7-pdo_pgsql
  + php7-pdo_sqlite
  + php7-pear
  + php7-pgsql
  + php7-phar
  + php7-posix
  + php7-session
  + php7-soap
  + php7-sockets
  + php7-sqlite3
  + php7-xml
  + php7-simplexml
  + php7-zip
  + php7-zlib
  + php7-tokenizer
  + php7-xmlwriter
  + nodejs
  + nodejs-npm
  + py2-pip
  + python3
  + mysql-client
  + redis
  + py-crcmod
  + libc6-compat
  + openssh-client
  + nfs-utils
  + iproute2
  + go


## Configurable envvars
~~~
CONFIG_VERBOSE="True"
CONFIG_GROUPS_MAIN_ID="1000"
CONFIG_GROUPS_MAIN_NAME="docker"
CONFIG_GROUPS_ADDITIONAL_ID="1001"
CONFIG_GROUPS_ADDITIONAL_NAME=""
CONFIG_USERS_MAIN_ID="1000"
CONFIG_USERS_MAIN_NAME="docker"
CONFIG_USERS_MAIN_GROUPS="docker"
CONFIG_USERS_ADDITIONAL_ID="1001"
CONFIG_USERS_ADDITIONAL_NAME=""
CONFIG_USERS_ADDITIONAL_GROUPS=""
CONFIG_READINESS_TEST="true"
CONFIG_LIVENESS_TEST="true"
CONFIG_PATHS_CONTAINER_STATUS="/tmp/container_status"
~~~
